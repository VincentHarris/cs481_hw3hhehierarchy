﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_Homework3Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class koopaPage : ContentPage
    {
        public koopaPage()
        {
            InitializeComponent();
        }

        void stompClick(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new stompPage());
        }
    }
}