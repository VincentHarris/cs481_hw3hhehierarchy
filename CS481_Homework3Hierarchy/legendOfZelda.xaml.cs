﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_Homework3Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class legendOfZelda : ContentPage
    {
        public legendOfZelda()
        {
            InitializeComponent();
        }

        void darkClick(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new darknessPage());
        }
    }
}