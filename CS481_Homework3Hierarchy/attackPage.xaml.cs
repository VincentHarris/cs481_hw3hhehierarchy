﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_Homework3Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class attackPage : ContentPage
    {
        public attackPage()
        {
            InitializeComponent();
        }
        void accomplishedClick(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new missionAccomplished());
        }


        //This breaks visual studio's emulator for whatever reason

       // async void OnAppearing(object sender, System.EventArgs e)
       // {
       //    await DisplayAlert("Mission Accomplished", "Printing Results","Print Complete");
       // }
    }
}