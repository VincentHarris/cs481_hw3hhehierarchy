﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_Homework3Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class hooray : ContentPage
    {
        public hooray()
        {
            InitializeComponent();
        }

        //This takes you back the scene selector page, for that is the root. Taken from xaml documentation 
        //https://github.com/xamarin/xamarin-forms-samples/blob/master/Navigation/Hierarchical/WorkingWithNavigation/Page3Xaml.xaml
        async void OnRootPageButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }
    }
}