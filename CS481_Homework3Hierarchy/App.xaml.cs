﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_Homework3Hierarchy
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new Navpage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
