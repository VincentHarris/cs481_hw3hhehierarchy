﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_Homework3Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Navpage : ContentPage
    {//This is where the buttons' functions are called
     //Each button creates their new respective page with the function "Navigation.PushAsync(new titlepage());"
        public Navpage()
        {
            InitializeComponent();
        }

        void MetroidClick(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new superMetroid());
        }

        void MarioClick(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new superMario());
        }

        private void Button_Clicked(object sender, EventArgs e)
        {

        }
        
        void ZeldaClick(object sender, System.EventArgs e)
        {
         Navigation.PushAsync(new legendOfZelda());
        }

       
    }
}